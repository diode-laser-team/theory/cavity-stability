function ABCD = stretch(L)
ABCD = [ 1 L ; 0 1 ];
end