function ABCD = refract(n_1,n_2)
ABCD = [ 1 0 ; 0 n_1/n_2 ];
end