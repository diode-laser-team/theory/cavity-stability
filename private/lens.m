function ABCD = lens(f)
ABCD = [ 1 0 ; -1/f 1 ];
end