% Cavity geometry is a Z-shape: Top left end mirror is M1, the plane HR
% coated facet of the gain crystal. Bottom right end mirror is M4, a plane
% PR coated mirror. Top right is M2, bottom left is M3, two HR curved
% mirrors. KL (Kerr lens) crystal is placed between the two curved mirrors.

P.lambda = 1064e-9; % [m] Wavelength
P.n_gaincrystal = 1.83; % RI of the gain crystal (Nd:YAG)
P.n_KLcrystal = 1.83; % RI of the Kerr-lens crystal (also Nd:YAG)
P.L_gaincrystal = 2e-3; % [m] Length of gain crystal
P.L_KLcrystal = 2e-3; % [m] Length of Kerr-lens crystal
P.alpha = 5/180*pi; % [rad] Folding half-angle (angle from beam to normal vector of mirror surface)
P.R = 50e-3; % [m] M2 and M3 RoC (positive for concave mirror)

Pow = 1; % [W] Estimated cavity mean output power
R_OC = 0.98; % Power reflectivity of output coupler
P.Pow_c = Pow/(1-R_OC); % [W] Estimated mean circulating cavity power
P.tau = 10e-12; % [s] Estimated FWHM pulse duration

P.n_2 = 6.13e-20; % [m^2/W] Intensity-proportional nonlinear refractive index for YAG

maxIterations = 20; % Max times to iterate on each individual point in the L_1,L_2,L_3 parameter space to arrive at a converged set of KL x and y waists
wdiff_convergencecriterion = 1e-8;

calcType = 2; % 1: calculate 3D plots (time consuming), 2: visualize 3D plots and caustics

switch calcType
  case 1 % Calculate 3D stability regions
    N1 = 100;
    N2 = 200;
    N3 = 100;

    L_1 = linspace(400e-3,600e-3,N1); % Distance from M1 to M2
    L_2 = linspace(51.4e-3,53.0e-3,N2); % Distance from M2 to M3
    L_3 = linspace(500e-3,800e-3,N3); % Distance from M3 to M4

    oneoverq_M1_CW_x = 1i*NaN(N1,N2,N3);
    oneoverq_KL_CW_x = 1i*NaN(N1,N2,N3);
    oneoverq_M4_CW_x = 1i*NaN(N1,N2,N3);
    oneoverq_M1_ML_x = 1i*NaN(N1,N2,N3);
    oneoverq_KL_ML_x = 1i*NaN(N1,N2,N3);
    oneoverq_M4_ML_x = 1i*NaN(N1,N2,N3);
    oneoverq_M1_CW_y = 1i*NaN(N1,N2,N3);
    oneoverq_KL_CW_y = 1i*NaN(N1,N2,N3);
    oneoverq_M4_CW_y = 1i*NaN(N1,N2,N3);
    oneoverq_M1_ML_y = 1i*NaN(N1,N2,N3);
    oneoverq_KL_ML_y = 1i*NaN(N1,N2,N3);
    oneoverq_M4_ML_y = 1i*NaN(N1,N2,N3);

    %% First CW
    tic
    for i=1:N1
      for j=1:N2
        for k=1:N3
          [oneoverq_M1_CW_x(i,j,k) , oneoverq_KL_CW_x(i,j,k) , oneoverq_M4_CW_x(i,j,k)] = calcStableOOQs(P,L_1(i),L_2(j),L_3(k),true,Inf,Inf);
          [oneoverq_M1_CW_y(i,j,k) , oneoverq_KL_CW_y(i,j,k) , oneoverq_M4_CW_y(i,j,k)] = calcStableOOQs(P,L_1(i),L_2(j),L_3(k),false,Inf,Inf);
          if isnan(oneoverq_M1_CW_x(i,j,k)) || isnan(oneoverq_M1_CW_y(i,j,k)) % Cavity is only stable if both axes are stable
            oneoverq_M1_CW_x(i,j,k) = NaN + NaN*1i;
            oneoverq_KL_CW_x(i,j,k) = NaN + NaN*1i;
            oneoverq_M4_CW_x(i,j,k) = NaN + NaN*1i;
            oneoverq_M1_CW_y(i,j,k) = NaN + NaN*1i;
            oneoverq_KL_CW_y(i,j,k) = NaN + NaN*1i;
            oneoverq_M4_CW_y(i,j,k) = NaN + NaN*1i;
          end
        end
      end
    end
    toc
    disp('Done with CW');
    
    w_0_KL_CW_x = sqrt(imag(1./oneoverq_KL_CW_x)*P.lambda/(pi*P.n_KLcrystal)); % Kerr crystal x waist size
    w_0_KL_CW_y = sqrt(imag(1./oneoverq_KL_CW_y)*P.lambda/(pi*P.n_KLcrystal)); % Kerr crystal y waist size
    
    %% Now with Kerr lens
    % Rather than using the spot size half-way between the curved mirrors
    % to calculate the strength of the Kerr lens, we use the waist. That's
    % because we imagine that we place the Kerr crystal in the focus
    % between the curved mirrors. We assume that the x and y focii are
    % close enough together that they both lie within the crystal. This may
    % not be a good approximation for cavity configurations with highly
    % elliptical/astigmatic modes.
    w_0_KL_ML_x_guesses = w_0_KL_CW_x; % Initial guess
    w_0_KL_ML_y_guesses = w_0_KL_CW_y; % Initial guess
    
    tested = false([N1 N2 N3]);
    newStabilitiesFound = true;
    CWinitialization = true;
    
    % We keep sweeping over all the points that have guesses for the KL
    % waists but have not yet been tested for stability, using the stable
    % points to make new guesses for the neighboring points for use in the
    % next sweep. We stop when no more stabilities have been found in a
    % sweep.
    tic;
    while newStabilitiesFound
      newStabilitiesFound = 0;
      for linidx=find(~isnan(w_0_KL_ML_x_guesses) & ~isnan(w_0_KL_ML_y_guesses) & ~tested).'
        [i,j,k] = ind2sub([N1 N2 N3],linidx);

        iteration = 1;
        wx_prevprev = NaN;
        wx_prev = NaN;
        wx = w_0_KL_ML_x_guesses(i,j,k);
        wy_prevprev = NaN;
        wy_prev = NaN;
        wy = w_0_KL_ML_x_guesses(i,j,k);
        while iteration <= maxIterations && ...
            ~(abs(wx - wx_prev) < wdiff_convergencecriterion && abs(wx_prev - wx_prevprev) < wdiff_convergencecriterion && abs(wx - wx_prev) < abs(wx_prev - wx_prevprev) && ...
              abs(wy - wy_prev) < wdiff_convergencecriterion && abs(wy_prev - wy_prevprev) < wdiff_convergencecriterion && abs(wy - wy_prev) < abs(wy_prev - wy_prevprev))
          if ~isnan(w_0_KL_ML_x_guesses(i,j,k)) && ~isnan(w_0_KL_ML_y_guesses(i,j,k))
            [oneoverq_M1_ML_x(i,j,k) , oneoverq_KL_ML_x(i,j,k) , oneoverq_M4_ML_x(i,j,k)] = calcStableOOQs(P,L_1(i),L_2(j),L_3(k),true,w_0_KL_ML_x_guesses(i,j,k),w_0_KL_ML_y_guesses(i,j,k));
            [oneoverq_M1_ML_y(i,j,k) , oneoverq_KL_ML_y(i,j,k) , oneoverq_M4_ML_y(i,j,k)] = calcStableOOQs(P,L_1(i),L_2(j),L_3(k),false,w_0_KL_ML_x_guesses(i,j,k),w_0_KL_ML_y_guesses(i,j,k));
            w_0_KL_ML_x_guesses(i,j,k) = sqrt(imag(1/oneoverq_KL_ML_x(i,j,k))*P.lambda/(pi*P.n_KLcrystal)); % Revised estimate of x KL waist
            w_0_KL_ML_y_guesses(i,j,k) = sqrt(imag(1/oneoverq_KL_ML_y(i,j,k))*P.lambda/(pi*P.n_KLcrystal)); % Revised estimate of y KL waist
          else
            w_0_KL_ML_x_guesses(i,j,k) = NaN;
            w_0_KL_ML_y_guesses(i,j,k) = NaN;
          end
          
          wx_prevprev = wx_prev;
          wx_prev = wx;
          wx = w_0_KL_ML_x_guesses(i,j,k);
          wy_prevprev = wy_prev;
          wy_prev = wy;
          wy = w_0_KL_ML_y_guesses(i,j,k);
          iteration = iteration + 1;
        end
        if iteration > maxIterations % w_0_KL diverged
          oneoverq_M1_ML_x(i,j,k) = NaN + NaN*1i;
          oneoverq_KL_ML_x(i,j,k) = NaN + NaN*1i;
          oneoverq_M4_ML_x(i,j,k) = NaN + NaN*1i;
          oneoverq_M1_ML_y(i,j,k) = NaN + NaN*1i;
          oneoverq_KL_ML_y(i,j,k) = NaN + NaN*1i;
          oneoverq_M4_ML_y(i,j,k) = NaN + NaN*1i;
        end
        
        if ~CWinitialization || ~isnan(oneoverq_M1_ML_x(i,j,k)) % Instabilities found during the initial CW-based guess run aren't enough to conclude that the cavity is unstable for ML here. For that we need to try again with a ML-based guess first.
          tested(i,j,k) = true;
        end
        if ~isnan(oneoverq_M1_ML_x(i,j,k))
          newStabilitiesFound = newStabilitiesFound + 1;
          for i_n = max(1,i-1):min(N1,i+1) % Neighbor i indices
            for j_n = max(1,j-1):min(N2,j+1) % Neighbor j indices
              for k_n = max(1,k-1):min(N3,k+1) % Neighbor k indices
                if ~tested(i_n,j_n,k_n)
                  w_0_KL_ML_x_guesses(i_n,j_n,k_n) = w_0_KL_ML_x_guesses(i,j,k);
                  w_0_KL_ML_y_guesses(i_n,j_n,k_n) = w_0_KL_ML_y_guesses(i,j,k);
                end
              end
            end
          end
        end
      end
      CWinitialization = false;
      fprintf('Found %d new stabilities this sweep\n',newStabilitiesFound);
    end
    toc
    disp('Done with ML');
    fprintf('Total stability points found: %d\n',sum(~isnan(oneoverq_M1_ML_x(:))));

    save('3Dstabilitydata.mat','P','L_1','L_2','L_3',...
      'oneoverq_M1_CW_x','oneoverq_M1_ML_x',...
      'oneoverq_KL_CW_x','oneoverq_KL_ML_x',...
      'oneoverq_M4_CW_x','oneoverq_M4_ML_x',...
      'oneoverq_M1_CW_y','oneoverq_M1_ML_y',...
      'oneoverq_KL_CW_y','oneoverq_KL_ML_y',...
      'oneoverq_M4_CW_y','oneoverq_M4_ML_y');
  case 2
    load('3Dstabilitydata.mat');
    
    q_M1_CW_x = cpxBeamPar(P.lambda,P.n_gaincrystal,1./oneoverq_M1_CW_x);
    q_M1_CW_y = cpxBeamPar(P.lambda,P.n_gaincrystal,1./oneoverq_M1_CW_y);
    q_KL_CW_x = cpxBeamPar(P.lambda,P.n_KLcrystal  ,1./oneoverq_KL_CW_x);
    q_KL_CW_y = cpxBeamPar(P.lambda,P.n_KLcrystal  ,1./oneoverq_KL_CW_y);
    q_M4_CW_x = cpxBeamPar(P.lambda,1              ,1./oneoverq_M4_CW_x);
    q_M4_CW_y = cpxBeamPar(P.lambda,1              ,1./oneoverq_M4_CW_y);

    q_M1_ML_x = cpxBeamPar(P.lambda,P.n_gaincrystal,1./oneoverq_M1_ML_x);
    q_M1_ML_y = cpxBeamPar(P.lambda,P.n_gaincrystal,1./oneoverq_M1_ML_y);
    q_KL_ML_x = cpxBeamPar(P.lambda,P.n_KLcrystal  ,1./oneoverq_KL_ML_x);
    q_KL_ML_y = cpxBeamPar(P.lambda,P.n_KLcrystal  ,1./oneoverq_KL_ML_y);
    q_M4_ML_x = cpxBeamPar(P.lambda,1              ,1./oneoverq_M4_ML_x);
    q_M4_ML_y = cpxBeamPar(P.lambda,1              ,1./oneoverq_M4_ML_y);

    %% Plot 3D
    slicepositions = [0.6 1 0];
    plotVolumetric(1,L_1*1e3,L_2*1e3,L_3*1e3,q_M1_CW_x.w*1e6,'slicepositions',slicepositions);
    xlabel('L_1 [mm]');ylabel('L_2 [mm]');zlabel('L_3 [mm]');
    caxis([0 1000]);colormap(GPBGYRcolormap)
    title('M1 x spot size CW [µm]');
    plotVolumetric(2,L_1*1e3,L_2*1e3,L_3*1e3,q_M1_CW_y.w*1e6,'slicepositions',slicepositions);
    xlabel('L_1 [mm]');ylabel('L_2 [mm]');zlabel('L_3 [mm]');
    caxis([0 1000]);colormap(GPBGYRcolormap)
    title('M1 y spot size CW [µm]');
    plotVolumetric(3,L_1*1e3,L_2*1e3,L_3*1e3,q_M1_ML_x.w*1e6,'slicepositions',slicepositions);
    xlabel('L_1 [mm]');ylabel('L_2 [mm]');zlabel('L_3 [mm]');
    caxis([0 1000]);colormap(GPBGYRcolormap)
    title('M1 x spot size ML [µm]');
    plotVolumetric(4,L_1*1e3,L_2*1e3,L_3*1e3,q_M1_ML_y.w*1e6,'slicepositions',slicepositions);
    xlabel('L_1 [mm]');ylabel('L_2 [mm]');zlabel('L_3 [mm]');
    caxis([0 1000]);colormap(GPBGYRcolormap)
    title('M1 y spot size ML [µm]');
    plotVolumetric(5,L_1*1e3,L_2*1e3,L_3*1e3,q_M1_CW_x.w./q_M1_ML_x.w,'slicepositions',slicepositions);
    xlabel('L_1 [mm]');ylabel('L_2 [mm]');zlabel('L_3 [mm]');
    caxis([0.5 2]);colormap(GPBGYRcolormap)
    title('M1 x spot size ratio CW/ML [µm]');
    plotVolumetric(6,L_1*1e3,L_2*1e3,L_3*1e3,q_M1_CW_y.w./q_M1_ML_y.w,'slicepositions',slicepositions);
    xlabel('L_1 [mm]');ylabel('L_2 [mm]');zlabel('L_3 [mm]');
    caxis([0.5 2]);colormap(GPBGYRcolormap)
    title('M1 y spot size ratio CW/ML [µm]');

    plotVolumetric(7,L_1*1e3,L_2*1e3,L_3*1e3,q_M4_CW_x.w*1e6,'slicepositions',slicepositions);
    xlabel('L_1 [mm]');ylabel('L_2 [mm]');zlabel('L_3 [mm]');
    caxis([0 1000]);colormap(GPBGYRcolormap)
    title('M4 x spot size CW [µm]');
    plotVolumetric(8,L_1*1e3,L_2*1e3,L_3*1e3,q_M4_CW_y.w*1e6,'slicepositions',slicepositions);
    xlabel('L_1 [mm]');ylabel('L_2 [mm]');zlabel('L_3 [mm]');
    caxis([0 1000]);colormap(GPBGYRcolormap)
    title('M4 y spot size CW [µm]');
    plotVolumetric(9,L_1*1e3,L_2*1e3,L_3*1e3,q_M4_ML_x.w*1e6,'slicepositions',slicepositions);
    xlabel('L_1 [mm]');ylabel('L_2 [mm]');zlabel('L_3 [mm]');
    caxis([0 1000]);colormap(GPBGYRcolormap)
    title('M4 x spot size ML [µm]');
    plotVolumetric(10,L_1*1e3,L_2*1e3,L_3*1e3,q_M4_ML_y.w*1e6,'slicepositions',slicepositions);
    xlabel('L_1 [mm]');ylabel('L_2 [mm]');zlabel('L_3 [mm]');
    caxis([0 1000]);colormap(GPBGYRcolormap)
    title('M4 y spot size ML [µm]');
    plotVolumetric(11,L_1*1e3,L_2*1e3,L_3*1e3,q_M4_CW_x.w./q_M4_ML_x.w,'slicepositions',slicepositions);
    xlabel('L_1 [mm]');ylabel('L_2 [mm]');zlabel('L_3 [mm]');
    caxis([0.5 2]);colormap(GPBGYRcolormap)
    title('M4 x spot size ratio CW/ML [µm]');
    plotVolumetric(12,L_1*1e3,L_2*1e3,L_3*1e3,q_M4_CW_y.w./q_M4_ML_y.w,'slicepositions',slicepositions);
    xlabel('L_1 [mm]');ylabel('L_2 [mm]');zlabel('L_3 [mm]');
    caxis([0.5 2]);colormap(GPBGYRcolormap)
    title('M4 y spot size ratio CW/ML [µm]');

%     plotVolumetric(13,L_1*1e3,L_2*1e3,L_3*1e3,q_KL_CW_x.w_0*1e6,'slicepositions',slicepositions);
%     xlabel('L_1 [mm]');ylabel('L_2 [mm]');zlabel('L_3 [mm]');
%     caxis([0 25]);colormap(GPBGYRcolormap)
%     title('KL x waist CW [µm]');
%     plotVolumetric(14,L_1*1e3,L_2*1e3,L_3*1e3,q_KL_CW_y.w_0*1e6,'slicepositions',slicepositions);
%     xlabel('L_1 [mm]');ylabel('L_2 [mm]');zlabel('L_3 [mm]');
%     caxis([0 25]);colormap(GPBGYRcolormap)
%     title('KL y waist CW [µm]');
%     plotVolumetric(15,L_1*1e3,L_2*1e3,L_3*1e3,q_KL_ML_x.w_0*1e6,'slicepositions',slicepositions);
%     xlabel('L_1 [mm]');ylabel('L_2 [mm]');zlabel('L_3 [mm]');
%     caxis([0 25]);colormap(GPBGYRcolormap)
%     title('KL x waist ML [µm]');
%     plotVolumetric(16,L_1*1e3,L_2*1e3,L_3*1e3,q_KL_ML_y.w_0*1e6,'slicepositions',slicepositions);
%     xlabel('L_1 [mm]');ylabel('L_2 [mm]');zlabel('L_3 [mm]');
%     caxis([0 100]);colormap(GPBGYRcolormap)
%     title('KL y waist ML [µm]');

    w_M1_x_onlyML = q_M1_ML_x.w;
    w_M1_x_onlyML(~isnan(q_M1_CW_x.w)) = NaN;
    plotVolumetric(17,L_1*1e3,L_2*1e3,L_3*1e3,w_M1_x_onlyML*1e6,'slicepositions',slicepositions);
    xlabel('L_1 [mm]');ylabel('L_2 [mm]');zlabel('L_3 [mm]');
    caxis([0 1000]);colormap(GPBGYRcolormap)
    title('M1 x spot size in ML, excluding CW stability regions [µm]');
%     w_M1_y_onlyML = q_M1_ML_y.w;
%     w_M1_y_onlyML(~isnan(q_M1_CW_y.w)) = NaN;
%     plotVolumetric(18,L_1*1e3,L_2*1e3,L_3*1e3,w_M1_y_onlyML*1e6,'slicepositions',slicepositions);
%     xlabel('L_1 [mm]');ylabel('L_2 [mm]');zlabel('L_3 [mm]');
%     caxis([0 1000]);colormap(GPBGYRcolormap)
%     title('M1 y spot size in ML, excluding CW stability regions [µm]');

    %% Plot 1D line
    L_1_ofinterest = 0.520; % [m]
    L_3_ofinterest = 0.570; % [m]

    [~,i1] = min(abs(L_1 - L_1_ofinterest));
    [~,i3] = min(abs(L_3 - L_3_ofinterest));
    w_M1_CW_x_plotdata = q_M1_CW_x.w(i1,:,i3).';
    w_M4_CW_x_plotdata = q_M4_CW_x.w(i1,:,i3).';
    w_M1_ML_x_plotdata = q_M1_ML_x.w(i1,:,i3).';
    w_M4_ML_x_plotdata = q_M4_ML_x.w(i1,:,i3).';
    w_M1_CW_y_plotdata = q_M1_CW_y.w(i1,:,i3).';
    w_M4_CW_y_plotdata = q_M4_CW_y.w(i1,:,i3).';
    w_M1_ML_y_plotdata = q_M1_ML_y.w(i1,:,i3).';
    w_M4_ML_y_plotdata = q_M4_ML_y.w(i1,:,i3).';

    figure(19);
    h_ax = subplot(1,2,1);
    plot(L_2*1e3,1e3*[w_M1_CW_x_plotdata w_M1_ML_x_plotdata w_M1_CW_y_plotdata w_M1_ML_y_plotdata],'linewidth',2);
    grid on; grid minor;
    xlim('tight');
    xlabel('L_2 [mm]');
    ylabel('M1 spot size [mm]');
    h_ax.ColorOrder = lines(2);
    h_ax.LineStyleOrder = {'-',':'};
    legend('Horz M1 CW','Horz M1 ML','Vert M1 CW','Vert M1 ML');
    title(['L_1 = ' num2str(L_1(i1)*1e3,5) ' mm, L_3 = ' num2str(L_3(i3)*1e3,5) ' mm']);
    
    h_ax = subplot(1,2,2);
    plot(L_2*1e3,1e3*[w_M4_CW_x_plotdata w_M4_ML_x_plotdata w_M4_CW_y_plotdata w_M4_ML_y_plotdata],'linewidth',2);
    grid on; grid minor;
    xlim('tight');
    xlabel('L_2 [mm]');
    ylabel('M4 spot size [mm]');
    h_ax.ColorOrder = lines(2);
    h_ax.LineStyleOrder = {'-',':'};
    legend('Horz M4 CW','Horz M4 ML','Vert M4 CW','Vert M4 ML');
    title(['L_1 = ' num2str(L_1(i1)*1e3,5) ' mm, L_3 = ' num2str(L_3(i3)*1e3,5) ' mm']);

    %% Calculate and plot caustics
    L_caustics_array = 1e-3*[...
      520 52.5475 570;
      520 52.3212 570;
      ];
    
    for i=1:size(L_caustics_array,1) % Round coordinates to match the nearest point in the grid
      figure(19+i);clf; h_ax = axes;

      [~,minidx1] = min(abs(L_caustics_array(i,1) - L_1));
      [~,minidx2] = min(abs(L_caustics_array(i,2) - L_2));
      [~,minidx3] = min(abs(L_caustics_array(i,3) - L_3));
      w_x_KL_guess = q_KL_ML_x.w_0(minidx1,minidx2,minidx3);
      w_y_KL_guess = q_KL_ML_y.w_0(minidx1,minidx2,minidx3);

      [distances , w_CW_x] = calcCaustic(P,L_1(minidx1),L_2(minidx2),L_3(minidx3),false,Inf,Inf);
      [~         , w_CW_y] = calcCaustic(P,L_1(minidx1),L_2(minidx2),L_3(minidx3),true,Inf,Inf);
      if any(isnan(w_CW_x)) || any(isnan(w_CW_y))
        w_CW_x(:) = NaN;
        w_CW_y(:) = NaN;
      end
      [~         , w_ML_x] = calcCaustic(P,L_1(minidx1),L_2(minidx2),L_3(minidx3),false,w_x_KL_guess,w_y_KL_guess);
      [~         , w_ML_y] = calcCaustic(P,L_1(minidx1),L_2(minidx2),L_3(minidx3),true,w_x_KL_guess,w_y_KL_guess);
      plot(distances*1e3,[w_CW_x.' w_ML_x.' w_CW_y.' w_ML_y.']*1e6,'linewidth',2);
      h_ax.ColorOrder = lines(2);
      h_ax.LineStyleOrder = {'-',':'};
      title(['Stable cavity mode caustics, L_1 = ' num2str(L_1(minidx1)*1e3,5) ' mm, L_2 = ' num2str(L_2(minidx2)*1e3,5) ' mm, L_3 = ' num2str(L_3(minidx3)*1e3,5) ' mm']);
      xlabel('Distance [mm]');ylabel('1/e^2 radius [µm]');
      legend('Horz M4 CW','Horz M4 ML','Vert M4 CW','Vert M4 ML','location','southeast');
      ylims = ylim;
      ylim([0 ylims(2)]);
      grid on;grid minor;
    end
end

function [distances , w] = calcCaustic(P,L_1,L_2,L_3,horizontalaxis,w_x_KL_guess,w_y_KL_guess)
c = 2.99792458e8; % [m/s] Speed of light in vacuum

f_rep = c/2/(L_1 + L_2 + L_3 + (P.n_gaincrystal-1)*P.L_gaincrystal + (P.n_KLcrystal - 1)*P.L_KLcrystal); % [Hz] Pulse repetition rate
% E_pulse = Pow/f_rep; % [J] Estimated output pulse energy
E_c_pulse = P.Pow_c/f_rep; % [J] Estimated intracavity pulse energy
P_c_peak = E_c_pulse/P.tau; % [W] Estimated intracavity peak pulse power

if horizontalaxis % The horizontal axis for this Z shape lies in the tangential plane also known as the meridional plane
  f = P.R/2*cos(P.alpha); % Curved mirrors
  f_KL = pi*w_x_KL_guess^3*w_y_KL_guess/(8*P.n_2*P.L_KLcrystal*P_c_peak); % Kerr lens effective focal length. Note that this assumes that the spot size is nearly unchanged over the length of the crystal.
else % The vertical axis lies in the sagittal plane
  f = P.R/2/cos(P.alpha); % Curved mirrors
  f_KL = pi*w_y_KL_guess^3*w_x_KL_guess/(8*P.n_2*P.L_KLcrystal*P_c_peak); % Kerr lens effective focal length. Note that this assumes that the spot size is nearly unchanged over the length of the crystal.
end

prop = ABCDpropagation(P.n_gaincrystal,... % In this kind of sequence, we place the first encountered optical element at the beginning of the cell array, unlike a sequence of multiplications of ABCD matrices in which the first element is the last one (furthest to the right).
      stretch(P.L_gaincrystal),...
      refract(P.n_gaincrystal,1),...
      stretch(L_1 - P.L_gaincrystal),...
      lens(f),...
      stretch(L_2/2 - P.L_KLcrystal/2),...
      refract(1,P.n_KLcrystal),...
      stretch(P.L_KLcrystal/2),...
      lens(f_KL),...
      stretch(P.L_KLcrystal/2),...
      refract(P.n_KLcrystal,1),...
      stretch(L_2/2 - P.L_KLcrystal/2),...
      lens(f),...
      stretch(L_3),...
      stretch(L_3),... % Here we begin the return trip
      lens(f),...
      stretch(L_2/2 - P.L_KLcrystal/2),...
      refract(1,P.n_KLcrystal),...
      stretch(P.L_KLcrystal/2),...
      lens(f_KL),...
      stretch(P.L_KLcrystal/2),...
      refract(P.n_KLcrystal,1),...
      stretch(L_2/2 - P.L_KLcrystal/2),...
      lens(f),...
      stretch(L_1 - P.L_gaincrystal),...
      refract(1,P.n_gaincrystal),...
      stretch(P.L_gaincrystal));

q = calcSelfRepeatingq(prop);
% if isnan(q)
%   warning('Failed to find a cavity stability');
% end
q_obj = cpxBeamPar(P.lambda,P.n_gaincrystal,q);
[distances,w] = calcSpotsizes(prop,10000,q_obj);
end

function [oneoverq_M1_CW , oneoverq_KL_CW , oneoverq_M4_CW] = calcStableOOQs(P,L_1,L_2,L_3,horizontalaxis,w_x_KL_guess,w_y_KL_guess)
c = 2.99792458e8; % [m/s] Speed of light in vacuum

f_rep = c/2/(L_1 + L_2 + L_3 + (P.n_gaincrystal-1)*P.L_gaincrystal + (P.n_KLcrystal - 1)*P.L_KLcrystal); % [Hz] Pulse repetition rate
% E_pulse = Pow/f_rep; % [J] Estimated output pulse energy
E_c_pulse = P.Pow_c/f_rep; % [J] Estimated intracavity pulse energy
P_c_peak = E_c_pulse/P.tau; % [W] Estimated intracavity peak pulse power

if horizontalaxis % The horizontal axis for this Z shape lies in the tangential plane also known as the meridional plane
  f = P.R/2*cos(P.alpha); % Curved mirrors
  f_KL = pi*w_x_KL_guess^3*w_y_KL_guess/(8*P.n_2*P.L_KLcrystal*P_c_peak); % Kerr lens effective focal length. Note that this assumes that the spot size is nearly unchanged over the length of the crystal.
else % The vertical axis lies in the sagittal plane
  f = P.R/2/cos(P.alpha); % Curved mirrors
  f_KL = pi*w_y_KL_guess^3*w_x_KL_guess/(8*P.n_2*P.L_KLcrystal*P_c_peak); % Kerr lens effective focal length. Note that this assumes that the spot size is nearly unchanged over the length of the crystal.
end

% From M1 to KL crystal
ABCD_M1_KL = stretch(P.L_KLcrystal/2)*...
             refract(1,P.n_KLcrystal)*...
             stretch(L_2/2 - P.L_KLcrystal/2)*...
             lens(f)*...
             stretch(L_1 - P.L_gaincrystal)*...
             refract(P.n_gaincrystal,1)*...
             stretch(P.L_gaincrystal);
A_1 = ABCD_M1_KL(1,1); B_1 = ABCD_M1_KL(1,2); C_1 = ABCD_M1_KL(2,1); D_1 = ABCD_M1_KL(2,2);
% From KL crystal to M4
ABCD_KL_M4 = stretch(L_3)*...
             lens(f)*...
             stretch(L_2/2 - P.L_KLcrystal/2)*...
             refract(P.n_KLcrystal,1)*...
             stretch(P.L_KLcrystal/2)*...
             lens(f_KL);
A_2 = ABCD_KL_M4(1,1); B_2 = ABCD_KL_M4(1,2); C_2 = ABCD_KL_M4(2,1); D_2 = ABCD_KL_M4(2,2);
% Back again from M4 to M1
ABCD_M4_M1 = stretch(P.L_gaincrystal)*...
             refract(1,P.n_gaincrystal)*...
             stretch(L_1 - P.L_gaincrystal)*...
             lens(f)*...
             stretch(L_2/2 - P.L_KLcrystal/2)*...
             refract(P.n_KLcrystal,1)*...
             stretch(P.L_KLcrystal/2)*...
             lens(f_KL)*...
             stretch(P.L_KLcrystal/2)*...
             refract(1,P.n_KLcrystal)*...
             stretch(L_2/2 - P.L_KLcrystal/2)*...
             lens(f)*...
             stretch(L_3);

ABCD = ABCD_M4_M1*ABCD_KL_M4*ABCD_M1_KL;

A = ABCD(1,1); B = ABCD(1,2); D = ABCD(2,2);

if abs(A + D) < 2 % Stability condition
  oneoverq_M1_CW = (D-A)/(2*B) - 1i/(2*abs(B))*sqrt(4-(A+D)^2);
  oneoverq_KL_CW = (C_1 + D_1*oneoverq_M1_CW)/(A_1 + B_1*oneoverq_M1_CW);
  oneoverq_M4_CW = (C_2 + D_2*oneoverq_KL_CW)/(A_2 + B_2*oneoverq_KL_CW);
else
  oneoverq_M1_CW = NaN + NaN*1i;
  oneoverq_KL_CW = NaN + NaN*1i;
  oneoverq_M4_CW = NaN + NaN*1i;
end
end
