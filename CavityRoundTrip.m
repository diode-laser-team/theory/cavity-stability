function CavityRoundTrip(lambda)

lambda = 1064e-9;
c = 2.99792458e8;
n_gaincrystal = 1.83; % RI of the gain crystal (Nd:YAG)
n_KLcrystal = 1.83; % RI of the Kerr-lens crystal (also Nd:YAG)

initial_waist = 31.1e-6; % m
position_relative_to_focus = 0; % m

z_R = pi*initial_waist^2/lambda*n_crystal; % Rayleigh range

q_i = position_relative_to_focus + i*z_R;

n_air = 1;

L_big = 107.6e-3;

L_small = 62.0e-3;

L_crystal = 18e-3;

folding_angle = 10/180*pi;

M3_M4_focal_length = 25e-3;

% First element, a stretch to crystal surface:
L_1 = L_crystal/2;

% Second element, a refraction out of crystal

% Third element, stretch to M4:
L_2 = (L_small-L_crystal)/2;

% Fourth element, reflection off curved surface (as lens):
f_1 = M3_M4_focal_length;

% Fifth element, stretch to M1:
L_3 = L_big/2 / cos(folding_angle) + L_small/2 / cos(folding_angle);

% Sixth element, stretch to M2:
L_4 = L_big;

% Seventh element. stretch to M3:
L_5 = L_big/2 / cos(folding_angle) + L_small/2 / cos(folding_angle);

% Eighth element, reflection off curved surface (as lens):
f_2 = M3_M4_focal_length;

% Ninth element, stretch to crystal surface:
L_6 = (L_small-L_crystal)/2;

% Tenth element, a refraction into crystal

% Eleventh element, a stretch to crystal center:
L_7 = L_crystal/2;

distances_to_calculate = linspace(0,1000e-3,1000);

spotsizes = zeros(1,length(distances_to_calculate));

for k=1:length(distances_to_calculate)
    remaining_distance = distances_to_calculate(k);
    
    ABCD = eye(2);
    element_to_append = 1;
    n = n_crystal;
    
    %Construct ABCD matrix for this distance
    while remaining_distance>0
        switch element_to_append
            case 1
                ABCD = stretch(min(remaining_distance,L_1))*ABCD;
                remaining_distance = remaining_distance - min(remaining_distance,L_1);
            case 2
                ABCD = refract(n,n_air)*ABCD;
                n = n_air;
            case 3
                ABCD = stretch(min(remaining_distance,L_2))*ABCD;
                remaining_distance = remaining_distance - min(remaining_distance,L_2);
            case 4
                ABCD = lens(f_1)*ABCD;
            case 5
                ABCD = stretch(min(remaining_distance,L_3))*ABCD;
                remaining_distance = remaining_distance - min(remaining_distance,L_3);
            case 6
                ABCD = stretch(min(remaining_distance,L_4))*ABCD;
                remaining_distance = remaining_distance - min(remaining_distance,L_4);
            case 7
                ABCD = stretch(min(remaining_distance,L_5))*ABCD;
                remaining_distance = remaining_distance - min(remaining_distance,L_5);
            case 8
                ABCD = lens(f_2)*ABCD;
            case 9
                ABCD = stretch(min(remaining_distance,L_6))*ABCD;
                remaining_distance = remaining_distance - min(remaining_distance,L_6);
            case 10
                ABCD = refract(n,n_crystal)*ABCD;
                n = n_crystal;
            case 11
                ABCD = stretch(min(remaining_distance,L_7))*ABCD;
                remaining_distance = remaining_distance - min(remaining_distance,L_7);
            otherwise
                break
        end
        element_to_append = element_to_append + 1;
        if element_to_append == 12;
            element_to_append = 1;
        end
    end
    q_f = (ABCD(1,1)*q_i + ABCD(1,2))/(ABCD(2,1)*q_i+ABCD(2,2));
    spotsizes(k) = sqrt(-lambda/pi/n/imag(1/q_f));
    
end
figure(1);
plot(distances_to_calculate,spotsizes)
y_limits = ylim;
ylim([0 y_limits(2)]);




end

function T = stretch(L);
T = [ 1 L ; 0 1 ];
end

function T = lens(f);
T = [ 1 0 ; -1/f 1 ];
end

function T = refract(n_1,n_2);
T = [ 1 0 ; 0 n_1/n_2 ];
end