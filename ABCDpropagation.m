classdef ABCDpropagation
  properties (SetAccess = private)
    ABCDsequence (2,2,:) double {mustBeReal} = [1 0 ; 0 1] % Each page must be a primitive ABCD propagation (a stretch, lens or refraction), nothing else is allowed
    n_start (1,1) double {mustBeReal} = 1 % Initial RI
  end
  properties (Dependent)
    n_end
    ABCD
  end
  methods
    %% Constructor
    function prop = ABCDpropagation(n_start,ABCD,varargin)
      if nargin > 0
        prop.ABCDsequence = cat(3,ABCD,varargin{:});
        prop.n_start = n_start;
        for iABCD = 1:size(prop.ABCDsequence,3)
          assert((prop.ABCDsequence(1,1,iABCD) == 1 && prop.ABCDsequence(2,1,iABCD) == 0 && prop.ABCDsequence(2,2,iABCD) == 1) || ... % stretch
                 (prop.ABCDsequence(1,1,iABCD) == 1 && prop.ABCDsequence(1,2,iABCD) == 0 && prop.ABCDsequence(2,2,iABCD) == 1) || ... % lens
                 (prop.ABCDsequence(1,1,iABCD) == 1 && prop.ABCDsequence(1,2,iABCD) == 0 && prop.ABCDsequence(2,1,iABCD) == 0),... % refraction
                 'Error: Input ABCD matrix #%d is not primitive (stretch, lens or refraction).',iABCD);
        end
      end
    end
    
    %% Dependent properties
    function n_end = get.n_end(prop)
      n_end = prop.n_start/prod(prop.ABCDsequence(2,2,:));
    end
    function ABCD = get.ABCD(prop)
      ABCD = prop.ABCDsequence(:,:,1);
      for iPage = 2:size(prop.ABCDsequence,3)
        ABCD = prop.ABCDsequence(:,:,iPage)*ABCD;
      end
    end
    
    %% Other methods
    function prop = reverse(prop)
      prop.n_start = prop.n_end;
      prop.ABCDsequence = flip(prop.ABCDsequence,3);
      for iElem = 1:size(prop.ABCDsequence,3)
        prop.ABCDsequence(2,2,iElem) = 1/prop.ABCDsequence(2,2,iElem); % Reverse direction of refractions
      end
    end
    
    function q = calcSelfRepeatingq(prop)
      ABCDmat = prop.ABCD;
      A = ABCDmat(1,1); B = ABCDmat(1,2); D = ABCDmat(2,2);
      if abs(A + D) < 2 % Stability condition
        q = 1/((D-A)/(2*B) - 1i/(2*abs(B))*sqrt(4-(A+D)^2));
      else
        q = NaN;
      end
    end
    
    function prop = combine(prop,prop_add)
      if abs(prop.n_end - prop_add.n_start) > 1e-10 % Test for equality while tolerating rounding errors
        error('Error: Final refractive index of propagation 1 does not match initial refractive index of propagation 2.')
      end
      prop.ABCDsequence = cat(3,prop.ABCDsequence,prop_add.ABCDsequence);
    end
    
    function [distances,w] = calcSpotsizes(prop,N,q,varargin)
      w = NaN(1,N);
      L = sum(prop.ABCDsequence(1,2,:));
      distances = linspace(0,L,N);
      if ~isnan(q.q)
        n = q.n;
        if numel(q.q) > 1
          if numel(varargin) < 1
            error('Error: When the complex beam parameter input contains an array of q values, the fourth argument must be the linear index of the specific q value to use for calculating spot sizes.');
          end
          q_prop = q.q(varargin{1});
        else
          q_prop = q.q;
        end
        distCovered = 0;
        for iElement = 1:size(prop.ABCDsequence,3)
          distElement = distCovered + prop.ABCDsequence(1,2,iElement);
          iDists = isnan(w) & distances <= distElement;
          w(iDists) = sqrt(-q.lambda_0./(pi*n*imag(1./(q_prop + distances(iDists) - distCovered))));
          ABCD_element = prop.ABCDsequence(:,:,iElement);
          q_prop = (ABCD_element(1,1)*q_prop + ABCD_element(1,2))/(ABCD_element(2,1)*q_prop + ABCD_element(2,2));
          n = n/ABCD_element(2,2);
          distCovered = distCovered + ABCD_element(1,2);
        end
      end
    end
  end
end