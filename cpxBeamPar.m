classdef cpxBeamPar
  properties (SetAccess = private)
    q double = complex(NaN)
    lambda_0 (1,1) double {mustBePositive} = 1
    n (1,1) double {mustBePositive} = 1
  end
  properties (Dependent)
    z
    z_R
    w_0
    ooq
    R
    w
  end
  methods
    %% Constructor
    function obj = cpxBeamPar(lambda_0,n,varargin)
      if nargin > 0
        obj.lambda_0 = lambda_0;
        obj.n = n;
        if numel(varargin) == 1
          obj.q = varargin{1};
        else
          z = varargin{1};
          assert(isreal(z));
          w_0 = varargin{2};
          assert(isreal(w_0) && all(~(w_0(:) <= 0)) && isequal(size(w_0),size(z)));
          obj.q = complex(z + 1i*pi*n*w_0.^2/lambda_0);
        end
      end
    end
    
    %% Dependent properties
    function val = get.z(obj)
      val = real(obj.q);
    end
    function val = get.z_R(obj)
      val = imag(obj.q);
    end
    function val = get.w_0(obj)
      val = sqrt(obj.z_R*obj.lambda_0/(pi*obj.n));
    end
    function val = get.ooq(obj)
      val = 1./obj.q;
    end
    function val = get.R(obj)
      val = 1./real(obj.ooq);
    end
    function val = get.w(obj)
      val = sqrt(-obj.lambda_0./(imag(obj.ooq)*pi*obj.n));
    end
    
    %% Other methods
    function obj = propagate(obj,prop)
      assert(prop.n_start == obj.n,'Error: The start refractive index of the propagation does not equal the refractive index of the complex beam parameter.');
      ABCDmat = prop.ABCD;
      obj.q = (ABCDmat(1,1)*obj.q + ABCDmat(1,2))./(ABCDmat(2,1)*obj.q + ABCDmat(2,2));
      obj.n = prop.n_end;
    end
  end
end