%% Reproduction of Kogelnik & Li fig. 4
lambda = 1000e-9;
g1 = linspace(-2,2,200);
g2 = linspace(-2,2,200);
f1 = 1./(1-g1)/2;
f2 = 1./(1-g2)/2;

oneoverq = 1i*NaN(numel(f1),numel(f2));
for i=1:numel(f1)
  for j=1:numel(f2)
    ABCD = stretch(1/2)*lens(f2(j))*stretch(1)*lens(f1(i))*stretch(1/2);
    A = ABCD(1,1); B = ABCD(1,2); C = ABCD(2,1); D = ABCD(2,2);
    
    if abs(A + D) < 2 % Stability condition
      oneoverq(i,j) = (D-A)/(2*B) - 1i/(2*abs(B))*sqrt(4-(A+D)^2);
    end
  end
end
n = 1; % At position of q calculation
w0 = sqrt(imag(1./oneoverq)*lambda/(pi*n));
focuspos = -real(1./oneoverq);

figure(1);clf;
subplot(1,2,1);
imagesc(g1,g2,w0.');
title('Cavity waist [m] for Fig. 4 of Kogelnik & Li');
xlabel('g_1 = 1-1/R_1');ylabel('g_2 = 1-1/R_2');caxis([0 1e-3]);colorbar;
axis xy equal tight
subplot(1,2,2);
imagesc(g1,g2,focuspos.');
title({'Focus offset from cavity midpoint [m] for Fig. 4 of Kogelnik & Li','Values outside the interval [-0.5 0.5] lie outside the cavity'});
xlabel('g_1 = 1-1/R_1');ylabel('g_2 = 1-1/R_2');caxis([-1 1]);colorbar;
axis xy equal tight

